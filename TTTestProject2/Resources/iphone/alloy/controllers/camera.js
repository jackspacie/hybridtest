function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "camera";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.newWindow = Ti.UI.createWindow({
        id: "newWindow"
    });
    $.__views.newWindow && $.addTopLevelView($.__views.newWindow);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var mainWin = Titanium.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical"
    });
    Titanium.UI.createView({
        height: "auto",
        width: "100%",
        top: 0,
        left: 0,
        right: 0,
        layout: "horizontal"
    });
    var titleLabel = Titanium.UI.createLabel({
        top: "70px",
        left: "95px",
        height: "50px",
        width: "200px",
        text: "Titanium"
    });
    var navView = Titanium.UI.createView({
        height: "140px",
        width: "100%",
        top: "-270px",
        left: 0,
        right: 0,
        layout: "horizontal",
        backgroundColor: "#11A7AB"
    });
    var bottomView = Titanium.UI.createView({
        height: "150px",
        width: "100%",
        top: "880px",
        left: 0,
        right: 0,
        layout: "horizontal",
        backgroundColor: "#3E4569"
    });
    var middleView = Titanium.UI.createView({
        height: "700px",
        width: "640px",
        top: "-880px",
        left: 0,
        right: 0,
        layout: "horizontal"
    });
    var backButton = Titanium.UI.createButton({
        color: "#000000",
        top: "60px",
        height: "auto",
        width: "80",
        title: "back",
        verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER
    });
    var cameraButton = Titanium.UI.createButton({
        color: "#E36500",
        top: "200px",
        height: "auto",
        width: "80",
        title: "take photo",
        verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER
    });
    Ti.App.addEventListener("closeWinB", function() {
        mainWin.close();
    });
    backButton.addEventListener("click", function() {
        Ti.API.info("Back Button was clicked");
        Ti.App.fireEvent("closeWinB");
    });
    cameraButton.addEventListener("click", function() {
        Ti.API.info("Back Button was clicked");
        Titanium.Media.showCamera({
            success: function(event) {
                Ti.API.debug("Our type was: " + event.mediaType);
                if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
                    var imageView = Ti.UI.createImageView({
                        width: mainWin.width,
                        height: mainWin.height,
                        image: event.media
                    });
                    middleView.add(imageView);
                } else alert("got the wrong type back =" + event.mediaType);
            },
            cancel: function() {},
            error: function(error) {
                var a = Titanium.UI.createAlertDialog({
                    title: "Camera"
                });
                error.code == Titanium.Media.NO_CAMERA ? a.setMessage("Please run this test on device") : a.setMessage("Unexpected error: " + error.code);
                a.show();
            },
            saveToPhotoGallery: true,
            allowEditing: true,
            mediaTypes: [ Ti.Media.MEDIA_TYPE_VIDEO, Ti.Media.MEDIA_TYPE_PHOTO ]
        });
    });
    navView.add(backButton);
    mainWin.add(cameraButton);
    mainWin.add(navView);
    mainWin.add(bottomView);
    mainWin.add(middleView);
    navView.add(titleLabel);
    mainWin.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;