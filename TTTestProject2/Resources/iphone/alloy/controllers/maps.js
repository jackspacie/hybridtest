function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "maps";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.newWindow = Ti.UI.createWindow({
        id: "newWindow"
    });
    $.__views.newWindow && $.addTopLevelView($.__views.newWindow);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var mainWin = Titanium.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical"
    });
    Titanium.UI.createView({
        height: "auto",
        width: "100%",
        top: 0,
        left: 0,
        right: 0,
        layout: "horizontal"
    });
    var titleLabel = Titanium.UI.createLabel({
        top: "70px",
        left: "95px",
        height: "50px",
        width: "200px",
        text: "Titanium"
    });
    var navView = Titanium.UI.createView({
        height: "140px",
        width: "100%",
        top: "-1145px",
        left: 0,
        right: 0,
        layout: "horizontal",
        backgroundColor: "#11A7AB"
    });
    var bottomView = Titanium.UI.createView({
        height: "150px",
        width: "100%",
        top: "880px",
        left: 0,
        right: 0,
        layout: "horizontal",
        backgroundColor: "#3E4569"
    });
    var backButton = Titanium.UI.createButton({
        color: "#000000",
        top: "60px",
        height: "auto",
        width: "80",
        title: "back"
    });
    Ti.App.addEventListener("closeWinB", function() {
        mainWin.close();
    });
    backButton.addEventListener("click", function() {
        Ti.API.info("Back Button was clicked");
        Ti.App.fireEvent("closeWinB");
    });
    var MapModule = require("ti.map");
    var mapview = MapModule.createView({
        mapType: MapModule.NORMAL_TYPE,
        top: "0px",
        left: "-170px"
    });
    navView.add(backButton);
    mainWin.add(mapview);
    mainWin.add(navView);
    mainWin.add(bottomView);
    navView.add(titleLabel);
    mainWin.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;