function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "flow2";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.newWindow = Ti.UI.createWindow({
        id: "newWindow"
    });
    $.__views.newWindow && $.addTopLevelView($.__views.newWindow);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var mainWin = Titanium.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical"
    });
    var view = Titanium.UI.createView({
        height: "auto",
        width: "100%",
        top: 0,
        left: 0,
        right: 0,
        layout: "horizontal"
    });
    var backButton = Titanium.UI.createButton({
        color: "#000000",
        top: "60px",
        height: "auto",
        width: "80",
        title: "back",
        verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER
    });
    backButton.addEventListener("click", function() {
        Ti.API.info("Back Button was clicked");
        Ti.App.fireEvent("closeWinB");
    });
    Ti.App.addEventListener("closeWinB", function() {
        mainWin.close();
    });
    var titleLabel = Titanium.UI.createLabel({
        top: "70px",
        left: "95px",
        height: "50px",
        width: "200px",
        text: "Titanium"
    });
    var navView = Titanium.UI.createView({
        height: "140px",
        width: "100%",
        top: "-270px",
        left: 0,
        right: 0,
        layout: "horizontal",
        backgroundColor: "#11A7AB"
    });
    var bottomView = Titanium.UI.createView({
        height: "150px",
        width: "100%",
        top: "880px",
        left: 0,
        right: 0,
        layout: "horizontal",
        backgroundColor: "#3E4569"
    });
    var button = Titanium.UI.createButton({
        color: "#E36500",
        top: "200px",
        left: "220px",
        height: "auto",
        width: "80",
        title: "flow3",
        verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER
    });
    button.addEventListener("click", function() {
        var w = Alloy.createController("flow3").getView("newWindow");
        w.show();
    });
    navView.add(backButton);
    view.add(button);
    view.add(navView);
    view.add(bottomView);
    navView.add(titleLabel);
    mainWin.add(view);
    mainWin.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;