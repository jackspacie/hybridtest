

var mainWin = Titanium.UI.createWindow({
    backgroundColor:'white',
    layout:'vertical'
});

var view = Titanium.UI.createView({
    height:'auto',
    width:'100%',
    top:0,
    left:0,
    right:0,
    layout:'horizontal'
 
});
 
var titleLabel = Titanium.UI.createLabel ({
	
	top:"70px",
	left: '255px',
	height:'50px',
	width:'200px',
	text:'Titanium'
});
 
 var navView = Titanium.UI.createView({
    height:'140px',
    width:'100%',
    top:"-570px",
    left:0,
    right:0,
    layout:'horizontal',
    backgroundColor:'#11A7AB'
 
});
 
 var bottomView = Titanium.UI.createView({
    height:'150px',
    width:'100%',
    top:"880px",
    left:0,
    right:0,
    layout:'horizontal',
    backgroundColor:'#3E4569'
 
}); 
 
var button = Titanium.UI.createButton({
	color: '#E36500',
	top: '200px',
	left:'220px',
	height:'auto',
	width:'80',
	title:'maps',
	verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
	   
});

 
var button2 = Titanium.UI.createButton({
	color: '#E36500',
	top: '300px',
	left:'-160px',
height:'auto',
width:'80',
title:'camera',
verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
    
}); 

var button3 = Titanium.UI.createButton({
	color: '#E36500',
	top: '40px',
	left: '220px',
height:'auto',
width:'80',
title:'image',
verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
   
}); 

var button4 = Titanium.UI.createButton({
	color: '#E36500',
	top: '40px',
	left: '220px',
height:'auto',
width:'80',
title:'flow',
verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
   
});


button.addEventListener('click', function()
{
	
	var w = Alloy.createController('maps').getView('newWindow');

	// create a button to close window
	
	w.show();//$.content.add(w);
});
 
button2.addEventListener('click', function()
{
	
	var w = Alloy.createController('camera').getView('newWindow');

	// create a button to close window
	
	w.show();//$.content.add(w);
});
 
 
button3.addEventListener('click', function()
{
	
	var w = Alloy.createController('images').getView('newWindow');

	// create a button to close window
	
	w.show();//$.content.add(w);
}); 

button4.addEventListener('click', function()
{
	
	var w = Alloy.createController('flow').getView('newWindow');

	// create a button to close window
	
	w.show();//$.content.add(w);
}); 
 
view.add(button);
view.add(button2);
view.add(button3);
view.add(button4);

view.add(navView);
view.add(bottomView);
navView.add(titleLabel);

mainWin.add(view);
 
mainWin.open();

