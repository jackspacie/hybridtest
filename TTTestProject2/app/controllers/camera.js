var mainWin = Titanium.UI.createWindow({
    backgroundColor:'white',
    layout:'vertical',
});

var view = Titanium.UI.createView({
    height:'auto',
    width:'100%',
    top:0,
    left:0,
    right:0,
    layout:'horizontal'
 
});

var titleLabel = Titanium.UI.createLabel ({
	
	top:"70px",
	left: '95px',
	height:'50px',
	width:'200px',
	text:'Titanium'
});
 
 var navView = Titanium.UI.createView({
    height:'140px',
    width:'100%',
    top:"-270px",
    left:0,
    right:0,
    layout:'horizontal',
    backgroundColor:'#11A7AB'
 
});
 
 var bottomView = Titanium.UI.createView({
    height:'150px',
    width:'100%',
    top:"880px",
    left:0,
    right:0,
    layout:'horizontal',
    backgroundColor:'#3E4569'
 
}); 

 var middleView = Titanium.UI.createView({
    height:'700px',
    width:'640px',
    top:"-880px",
    left:0,
    right:0,
    layout:'horizontal',
    
}); 

var backButton = Titanium.UI.createButton({
	color: '#000000',
	top: '60px',
	height:'auto',
	width:'80',
	title:'back',
	verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
});

var cameraButton = Titanium.UI.createButton({
	color: '#E36500',
	top: '200px',
	height:'auto',
	width:'80',
	title:'take photo',
	verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
});

 Ti.App.addEventListener('closeWinB',function(e){
    mainWin.close();
});

backButton.addEventListener('click', function(e){
    Ti.API.info("Back Button was clicked");
    
    Ti.App.fireEvent('closeWinB');
});
	
cameraButton.addEventListener('click', function(e){
    Ti.API.info("Back Button was clicked");
    
    // This example is only able to capture video on the iOS platform.
// To capture video on the Android platform, see the Android Capture Video Example below.
Titanium.Media.showCamera({
	success:function(event) {
		// called when media returned from the camera
		Ti.API.debug('Our type was: '+event.mediaType);
		if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
			var imageView = Ti.UI.createImageView({
				
				width:mainWin.width,
				height:mainWin.height,
				image:event.media
			});
			middleView.add(imageView);
		} else {
			alert("got the wrong type back ="+event.mediaType);
		}
	},
	cancel:function() {
		// called when user cancels taking a picture
	},
	error:function(error) {
		// called when there's an error
		var a = Titanium.UI.createAlertDialog({title:'Camera'});
		if (error.code == Titanium.Media.NO_CAMERA) {
			a.setMessage('Please run this test on device');
		} else {
			a.setMessage('Unexpected error: ' + error.code);
		}
		a.show();
	},
	saveToPhotoGallery:true,
    // allowEditing and mediaTypes are iOS-only settings
	allowEditing:true,
	mediaTypes:[Ti.Media.MEDIA_TYPE_VIDEO,Ti.Media.MEDIA_TYPE_PHOTO]
});
});

navView.add(backButton);

mainWin.add(cameraButton);

mainWin.add(navView);
mainWin.add(bottomView);
mainWin.add(middleView);

//mainWin.add(view);

navView.add(titleLabel);

mainWin.open();