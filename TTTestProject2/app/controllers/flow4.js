

var theWin = Titanium.UI.createWindow({
    backgroundColor:'white',
    layout:'vertical'
});

var view = Titanium.UI.createView({
    height:'auto',
    width:'100%',
    top:0,
    left:0,
    right:0,
    layout:'horizontal'
 
});
 
var titleLabel = Titanium.UI.createLabel ({
	
	top:"70px",
	left: '90px',
	height:'50px',
	width:'200px',
	text:'Titanium'
});
 
 var navView = Titanium.UI.createView({
    height:'140px',
    width:'100%',
    top:"-570px",
    left:0,
    right:0,
    layout:'horizontal',
    backgroundColor:'#11A7AB'
 
});
 
 var bottomView = Titanium.UI.createView({
    height:'150px',
    width:'100%',
    top:"880px",
    left:0,
    right:0,
    layout:'horizontal',
    backgroundColor:'#3E4569'
}); 
 
 var backButton = Titanium.UI.createButton({
	color: '#000000',
	top: '60px',
	height:'auto',
	width:'80',
	title:'back',
	verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
});

backButton.addEventListener('click', function(e){
    Ti.API.info("Back Button was clicked");
    
    Ti.App.fireEvent('closeWinB');
});
 
 Ti.App.addEventListener('closeWinB',function(e){
    theWin.close();
}); 

var button = Titanium.UI.createButton({
	color: '#E36500',
	top: '200px',
	left:'220px',
	height:'auto',
	width:'80',
	title:'button1',
	verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
	   
});

 
var button2 = Titanium.UI.createButton({
	color: '#E36500',
	top: '300px',
	left:'-160px',
height:'auto',
width:'80',
title:'button2',
verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
    
}); 

var button3 = Titanium.UI.createButton({
	color: '#E36500',
	top: '40px',
	left: '220px',
height:'auto',
width:'80',
title:'button3',
verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
   
}); 

var button4 = Titanium.UI.createButton({
	color: '#E36500',
	top: '40px',
	left: '220px',
height:'auto',
width:'80',
title:'button4',
verticalAlign: Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
   
});

 
view.add(button);
view.add(button2);
view.add(button3);
view.add(button4);

navView.add(backButton);
view.add(navView);
view.add(bottomView);
navView.add(titleLabel);

theWin.add(view);
 
theWin.open();

