//
//  main.m
//  NativeTestProject
//
//  Created by Spacie, Jack (UK - London) on 09/05/2014.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
