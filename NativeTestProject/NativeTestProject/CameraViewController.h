//
//  CameraViewController.h
//  NativeTestProject
//
//  Created by Spacie, Jack (UK - London) on 09/05/2014.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)takePhoto:  (UIButton *)sender;
- (IBAction)selectPhoto:(UIButton *)sender;


@end
