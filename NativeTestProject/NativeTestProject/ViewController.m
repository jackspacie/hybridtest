//
//  ViewController.m
//  NativeTestProject
//
//  Created by Spacie, Jack (UK - London) on 09/05/2014.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

#import "ViewController.h"
#import "APLTransitionController.h"
#import "APLStackLayout.h"
#import "APLStackCollectionViewController.h"

@interface ViewController () <UINavigationControllerDelegate, APLTransitionControllerDelegate>

@property (nonatomic) APLTransitionController *transitionController;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goCollection:(id)sender {
    APLStackLayout* stackLayout = [[APLStackLayout alloc] init];
    APLStackCollectionViewController* collectionViewController = [[APLStackCollectionViewController alloc] initWithCollectionViewLayout:stackLayout];
    collectionViewController.title = @"Stack Layout";
    self.navigationController.delegate = self;
    
    self.transitionController = [[APLTransitionController alloc] initWithCollectionView:collectionViewController.collectionView];
    self.transitionController.delegate = self;
    
    [self.navigationController pushViewController:collectionViewController animated:YES];
}

-(void)interactionBeganAtPoint:(CGPoint)p
{
    // Very basic communication between the transition controller and the top view controller
    // It would be easy to add more control, support pop, push or no-op
    UIViewController* viewController = [(APLCollectionViewController*)[self.navigationController topViewController] nextViewControllerAtPoint:p];
    if (viewController!=nil)
    {
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (id <UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                          interactionControllerForAnimationController:(id <UIViewControllerAnimatedTransitioning>) animationController
{
    if (animationController==self.transitionController)
    {
        return self.transitionController;
    }
    return nil;
}


- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC
{
    if (![fromVC isKindOfClass:[UICollectionViewController class]] || ![toVC isKindOfClass:[UICollectionViewController class]])
    {
        return nil;
    }
    if (!self.transitionController.hasActiveInteraction)
    {
        return nil;
    }
    
    self.transitionController.navigationOperation = operation;
    return self.transitionController;
}

@end
