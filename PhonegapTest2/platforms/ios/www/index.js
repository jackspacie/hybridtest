/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
showMap: function() {
    var pins = [[49.28115, -123.10450], [49.27503, -123.12138], [49.28286, -123.11891]];
    var error = function() {
        console.log('error');
    };
    var success = function() {
        mapKit.addMapPins(pins, function() {
                          console.log('adMapPins success');
                          },
                          function() { console.log('error'); });
    };
    mapKit.showMap(success, error);
},
hideMap: function() {
    var success = function() {
        console.log('Map hidden');
    };
    var error = function() {
        console.log('error');
    };
    mapKit.hideMap(success, error);
},
clearMapPins: function() {
    var success = function() {
        console.log('Map Pins cleared!');
    };
    var error = function() {
        console.log('error');
    };
    mapKit.clearMapPins(success, error);
},
changeMapType: function() {
    var success = function() {
        console.log('Map Type Changed');
    };
    var error = function() {
        console.log('error');
    };
    mapKit.changeMapType(mapKit.mapType.MAP_TYPE_SATELLITE, success, error);
}
}
